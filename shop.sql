/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-03-16 02:04:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT '/assets/images/no_image.jpg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Shoes', '/assets/images/generate/7865.jpg');
INSERT INTO `categories` VALUES ('2', 'Jeans ', '/assets/images/generate/3214.jpg');
INSERT INTO `categories` VALUES ('3', 'Hats', '/assets/images/generate/3217.jpg');
INSERT INTO `categories` VALUES ('4', 'Brassieres', '/assets/images/generate/9090.jpg');
INSERT INTO `categories` VALUES ('5', 'Coats', '/assets/images/generate/2d050418.jpeg');
INSERT INTO `categories` VALUES ('6', 'Shorts', '/assets/images/generate/5555.jpg');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `items` varchar(10000) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `summ` decimal(18,9) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('4', '2016-03-15 11:25:08', 'a:1:{i:0;a:6:{s:2:\"id\";s:1:\"7\";s:5:\"title\";s:25:\"Blue Denim Jean Brassiere\";s:5:\"price\";s:6:\"199.00\";s:8:\"quantity\";s:1:\"1\";s:5:\"total\";s:3:\"199\";s:5:\"photo\";s:41:\"/assets/images/generate/7743534534534.jpg\";}}', 'Ivanov', 'Dima', 'sergeich-81@inbox.ru', 'Samoletnaya 13/21 061320', '38066-199-10-23', '199.000000000');
INSERT INTO `orders` VALUES ('5', '2016-03-15 11:26:26', 'a:2:{i:0;a:6:{s:2:\"id\";s:1:\"5\";s:5:\"title\";s:38:\"Goorin Bros. Los Fox Wool-Blend Fedora\";s:5:\"price\";s:6:\"540.50\";s:8:\"quantity\";s:1:\"1\";s:5:\"total\";s:5:\"540.5\";s:5:\"photo\";s:37:\"/assets/images/generate/154409116.jpg\";}i:1;a:6:{s:2:\"id\";s:1:\"7\";s:5:\"title\";s:25:\"Blue Denim Jean Brassiere\";s:5:\"price\";s:6:\"199.00\";s:8:\"quantity\";s:1:\"2\";s:5:\"total\";s:3:\"398\";s:5:\"photo\";s:41:\"/assets/images/generate/7743534534534.jpg\";}}', 'Петрова', 'Анна', 'anna@web.com', 'Харьков ул. Иванова 12, кв. 5', '38099-126-56-75', '938.500000000');
INSERT INTO `orders` VALUES ('6', '2016-03-15 13:48:44', 'a:1:{i:0;a:6:{s:2:\"id\";s:1:\"5\";s:5:\"title\";s:38:\"Goorin Bros. Los Fox Wool-Blend Fedora\";s:5:\"price\";s:6:\"540.50\";s:8:\"quantity\";s:2:\"10\";s:5:\"total\";s:4:\"5405\";s:5:\"photo\";s:37:\"/assets/images/generate/154409116.jpg\";}}', 'Moor', 'John', 'john@yahoo.com.au', 'Lenin str. 12', '38095-128-98-98', '5405.000000000');

-- ----------------------------
-- Table structure for `product_category`
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `product_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  KEY `produkt_key` (`product_id`),
  KEY `category_key` (`category_id`),
  CONSTRAINT `category_key` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `produkt_key` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('4', '3');
INSERT INTO `product_category` VALUES ('5', '3');
INSERT INTO `product_category` VALUES ('6', '3');
INSERT INTO `product_category` VALUES ('7', '4');
INSERT INTO `product_category` VALUES ('7', '2');

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `photo` varchar(255) DEFAULT '/assets/images/no_image.jpg',
  `price` decimal(18,9) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('4', 'Weathered Cotton Military Cadet Cap', 'Duis placerat, nisi vulputate varius rutrum, ante orci dapibus arcu, quis auctor odio purus vel diam. Donec venenatis tincidunt finibus. Sed tortor velit, luctus in massa at, vehicula imperdiet dolor. Proin sit amet dapibus nisl. Curabitur molestie sem vitae augue accumsan, eget imperdiet ligula tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum et lobortis enim. Vestibulum pulvinar vestibulum quam, in porta metus. Phasellus a ante id urna pretium viverra.', '/assets/images/generate/160125235.jpg', '770.000000000');
INSERT INTO `products` VALUES ('5', 'Goorin Bros. Los Fox Wool-Blend Fedora', 'Donec dui nisl, tristique non ornare vitae, fermentum ac libero. Nulla porta, mauris sed sodales tempor, nisl dui posuere urna, ac porta eros lorem nec felis. In hac habitasse platea dictumst. In porttitor sapien risus, nec accumsan lectus fermentum eget. ', '/assets/images/generate/154409116.jpg', '540.500000000');
INSERT INTO `products` VALUES ('6', 'Weathered Cotton Baseball Cap', 'Aliquam efficitur nisl est. Nulla non venenatis elit. Quisque vehicula orci at facilisis pretium. Donec tempus maximus orci ut volutpat. Cras quis tortor vel tellus faucibus condimentum. Sed eget enim mattis, congue tellus ac, blandit mi. Morbi volutpat et lacus eget viverra.', '/assets/images/generate/143020528.jpg', '325.000000000');
INSERT INTO `products` VALUES ('7', 'Blue Denim Jean Brassiere', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et tincidunt felis. Sed at venenatis turpis. Nunc tempor velit sem, ac sollicitudin lacus viverra non. Aenean tincidunt arcu sit amet sodales mollis. Proin nec venenatis arcu, at dictum leo. Integer tincidunt vitae elit nec rutrum.', '/assets/images/generate/7743534534534.jpg', '180.300000000');

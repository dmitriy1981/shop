<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
 	'default' => array
 	(
 		'type'       => 'MySQLi',
 		'connection' => array(
 			'hostname'   => 'localhost',
 			'database'   => 'shop',
 			'username'   => 'root',
 			'password'   => 123,
 			'persistent' => FALSE,
 			'ssl'        => NULL,
 		),
 		'table_prefix' => '',
 		'charset'      => 'utf8',
 		'caching'      => FALSE,
 	),
);

<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(

	'driver'       => 'File',
	'hash_method'  => 'sha256',
	'hash_key'     => 'JHJKHSJ:LDNKHJKHJHJHJLKJLKJ',
	'lifetime'     => 1209600,
	'session_type' => Session::$default,
	'session_key'  => 'auth_user',

	// Username/password combinations for the Auth File driver
	'users' => array(
		 'admin' => 'bc5de149c6c767333b3e87f3f076df46c91e39c83e1f3d0264c8b1c331818fd2',
	),

);

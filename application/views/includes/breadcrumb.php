<div class="ui breadcrumb">
    <a href="/" class="section">Home</a>
    <?php if($catId != 0): ?>
    <i class="right chevron icon divider"></i>
    <a href="/category/" class="section">All products</a>
            <?php if(isset($product)): ?>
            <i class="right chevron icon divider"></i>
            <?php endif; ?>
    <?php endif; ?>
    
    <?php if(!isset($product)): ?>
    <i class="right arrow icon divider"></i>
    <div class="active section"><?php echo $categoryTitle; ?></div>
    <?php else: ?>
            <?php for($i = 0; $i < count($product['categories']); $i++): ?>
                
            <a href="/category/<?php echo $product['categories'][$i]['id']; ?>" class="section">
                <?php echo $product['categories'][$i]['name']; ?>
            </a>
                <?php if($i < (count($product['categories']) - 1)): ?>
                 	&nbsp; | &nbsp;
                <?php endif; ?>
            
            <?php endfor; ?>
            <i class="right arrow icon divider"></i>
            <div class="active section"><?php echo $product[0]['name']; ?></div>
    <?php endif; ?>
    
</div>
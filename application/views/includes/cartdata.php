    <span>
    <?php $totalSumm  = 0; ?>
    <?php $totalItems = 0; ?>
        <div class="ui grid">
            <div style="min-width: 400px" class="sixteen wide column aded-items">Recently added item(s)</div>
          <?php for($i = 0; $i < count($_SESSION['cart']); $i++): ?>
          <?php $totalSumm  += $_SESSION['cart'][$i]['total']; ?>
          <?php $totalItems += $_SESSION['cart'][$i]['quantity']; ?>
            <div class="three column row single-item">
              <div class="column">
                  <img style="width: 300px;" src="<?php echo $_SESSION['cart'][$i]['photo']; ?>" alt=""/>
              </div>
              <div class="column single-item-title">
                  <?php echo $_SESSION['cart'][$i]['title']; ?>
                  <hr class="title-separator">
                  <?php echo $_SESSION['cart'][$i]['quantity']; ?> 
                    &times; 
                  <?php echo $_SESSION['cart'][$i]['price']; ?>
                    <br>
                    <div class="item-total">
                        = <b><?php echo floatval($_SESSION['cart'][$i]['total']); ?></b> UAH
                    </div>
              </div>
              <div class="column delete-section">
                <button data-id="<?php echo $i; ?>" class="ui circular black mini icon button right floated">
                  <i class="remove icon"></i>
                </button>
              </div>
            </div>
          <?php endfor; ?>
        </div>
        <span id="hide-total" style="display: none;"><?php echo $totalItems; ?></span>
        <div class="ui grid">
            <div class="ten wide column total-summ">Total:<span> <?php echo $totalSumm; ?> <font>UAH</font></span> </div>
            <div class="six wide column checkout-button-block">
                <a href="/checkout" class="ui secondary button checkout-button">Checkout</a>
            </div>
        </div>
    </span>
  <div class="ui fixed inverted menu">
    <div class="ui container">
      <a href="/" class="header item">
        <img class="logo" src="<?php echo URL::base(); ?>assets/images/logo.png">
        SuperShop
      </a>
      <div class="ui simple dropdown item">
        Categories <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="/category"><b>All</b></a>
          <?php for($i = 0; $i < count($categories); $i++): ?>
          <a class="item" href="/category/<?php echo $categories[$i]['id']; ?>">
              <?php echo $categories[$i]['name']; ?>
          </a>
          <?php endfor; ?>
        </div>
      </div>
      <a href="/checkout" class="item">Checkout</a>
    </div>
      <a href="/cart" style="float: right; padding-right: 10px;" class="item bag" data-html="">
          <i class="add to cart icon"></i>Cart &nbsp;
          <b id="item-count">
              <?php if(!empty($_SESSION['cart'])){
                  
                $itemsCount = 0;
                   
                  for($j = 0; $j < count($_SESSION['cart']); $j++)
                  {
                      $itemsCount += $_SESSION['cart'][$j]['quantity'];
                  }
                  
                  echo '('.$itemsCount.')'; 
                  
              } ?>
          </b>
      </a>
  </div>
<!--- Shopping cart ---->
<div style="display: none" id="shopping-cart">
    <?php if(!empty($_SESSION['cart'])): ?>
    <span>
    <?php $totalSumm = 0; ?>
        <div class="ui grid">
            <div style="min-width: 400px" class="sixteen wide column aded-items">Recently added item(s)</div>
          <?php for($i = 0; $i < count($_SESSION['cart']); $i++): ?>
          <?php $totalSumm += $_SESSION['cart'][$i]['total']; ?>
            <div class="three column row single-item">
              <div class="column">
                  <img style="width: 300px;" src="<?php echo $_SESSION['cart'][$i]['photo']; ?>" alt=""/>
              </div>
              <div class="column single-item-title">
                  <?php echo $_SESSION['cart'][$i]['title']; ?>
                  <hr class="title-separator">
                  <?php echo $_SESSION['cart'][$i]['quantity']; ?> 
                    &times; 
                  <?php echo $_SESSION['cart'][$i]['price']; ?>
                    <br>
                    <div class="item-total">
                        = <b><?php echo floatval($_SESSION['cart'][$i]['total']); ?></b> UAH
                    </div>
              </div>
              <div class="column delete-section">
                <button data-id="<?php echo $i; ?>" class="ui circular black mini icon button right floated">
                  <i class="remove icon"></i>
                </button>
              </div>
            </div>
          <?php endfor; ?>
        </div>
        <div class="ui grid">
            <div class="ten wide column total-summ">Total:<span> <?php echo $totalSumm; ?> <font>UAH</font></span> </div>
            <div class="six wide column checkout-button-block">
                <a href="/checkout" class="ui secondary button checkout-button">Checkout</a>
            </div>
        </div>
    </span>
    <?php else: ?>
    <span>You have no items in your shopping cart</span>
    <?php endif; ?>
    
</div>
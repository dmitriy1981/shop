<h1 class="ui header">Online Shopping for Women & Men</h1>
    <p>Duis sollicitudin faucibus neque, sed iaculis lorem laoreet id.
        Fusce eleifend fermentum augue, et convallis quam vulputate et. 
        Nullam eu mauris eget ante interdum porta at eu elit.
    </p>
<div class="ui three column grid">
    <?php foreach ($categories AS $v): ?>
        <div class="column">
         <a class="item-link" href="/category/<?php echo $v['id']; ?>">
          <div class="ui segment">
            <img src="<?php echo $v['photo']; ?>" class="ui wireframe image">
          </div>
            <div class="category-name"><?php echo $v['name']; ?></div>
         </a>
        </div>
    <?php endforeach; ?>
</div>
<?php $catId = TRUE; ?>
<?php include Kohana::find_file('views/includes', 'breadcrumb'); ?>

<p id="item-block">
    <div class="ui grid">
      <div class="sixteen wide column"><h2 id="item-title"><?php echo $product[0]['name']; ?></h2></div>
      <div class="ten wide column">
          <div class="sixteen wide column item-photo-block">
              <img style="max-width: 300px;" class="ui medium rounded image" src="<?php echo $product[0]['photo']; ?>">
          </div>
          <div class="sixteen wide column item-description">
              <?php echo $product[0]['description']; ?>
          </div>
      </div>
      <div class="six wide column">
          <div id="item-id"><?php echo $product[0]['id']; ?></div>
          <a class="ui tag label item-price">
              <span id="item-value"><?php echo number_format($product[0]['price'], 2); ?></span>
              <span id="currency">UAH</span>
          </a>
          <div style="text-align: center" class="sixteen wide column add-to-cart">
              
            <div class="ui selection dropdown">
              <input value="1" name="quantity" type="hidden">
              <i class="dropdown icon"></i>
              <div class="default text">Quantity</div>
              <div class="menu">
                <div class="item" data-value="1">1</div>
                <div class="item" data-value="2">2</div>
                <div class="item" data-value="3">3</div>
                <div class="item" data-value="4">4</div>
                <div class="item" data-value="5">5</div>
                <div class="item" data-value="6">6</div>
                <div class="item" data-value="7">7</div>
                <div class="item" data-value="8">8</div>
                <div class="item" data-value="9">9</div>
                <div class="item" data-value="10">10</div>
              </div>
            </div>
              <button class="ui right labeled icon button secondary add-cart-button">
                <i class="right arrow icon"></i>
                Add to cart
              </button>
              
          </div>
      </div>
    </div>
</p>
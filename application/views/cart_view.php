<h1 class="ui header">Shopping Cart</h1>
<div class="ui grid">
 <?php if(!empty($_SESSION['cart'])): ?>  
  <div class="row cart-block-header">
    <div class="three wide column"></div>
    <div class="four wide column">Product Name</div>
    <div class="three wide column">Unit Price</div>
    <div class="two wide column">Qty</div>
    <div class="two wide column">Subtotal</div>
    <div class="two wide column"></div>
  </div>
    <?php $totalSumm = 0; ?>
    <?php for($i = 0; $i < count($_SESSION['cart']); $i++): ?>
    <?php $totalSumm += $_SESSION['cart'][$i]['total']; ?>
      <div class="row cart-block">
        <div class="three wide column">
            <img src="<?php echo $_SESSION['cart'][$i]['photo']; ?>" alt=""/>
        </div>
        <div class="four wide column"><?php echo $_SESSION['cart'][$i]['title']; ?></div>
        <div class="three wide column"><?php echo $_SESSION['cart'][$i]['price']; ?></div>
        <div class="two wide column">
               <div class="ui input focus mini mini-field">
                   <input maxlength="2" value="<?php echo $_SESSION['cart'][$i]['quantity']; ?>" placeholder="" type="text">
              </div>
        </div>
        <div class="two wide column"><?php echo floatval($_SESSION['cart'][$i]['total']); ?></div>
        <div class="two wide column delete-section">
                <button data-id="<?php echo $i; ?>" class="ui circular black mini icon button right floated">
                  <i class="remove icon"></i>
                </button>
        </div>
      </div>
    <?php endfor; ?>  
  <div class="row cart-block-footer">
    <div class="three wide column"></div>
    <div class="four wide column grand-total">Grand total: 
        <sum><?php echo $totalSumm; ?></sum> UAH
    </div>
    <div class="three wide column"></div>
    <div class="two wide column"></div>
    <div class="two wide column"><button class="ui tiny teal left floated button update-cart">Update</button></div>
    <div class="two wide column"></div>
  </div>
  <?php else: ?>
    <div class="row empty-cart">
        <h3>Shopping Cart is Empty</h3>
        <h6>You have no items in your shopping cart.</h6>
    </div>
  <?php endif; ?>  
</div>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title><?php echo $title; ?></title>
  <meta name="description" content="<?php echo $description; ?>">

    <?php foreach($styles as $style): ?>
        <link href="<?php echo URL::base(); ?>assets/css/<?php echo $style; ?>.css" 
        rel="stylesheet" type="text/css" />
    <?php endforeach; ?>


</head>
<body>

<?php include Kohana::find_file('views/includes', 'navigation'); ?>  

  <div class="ui main text container">
      <?php echo $content; ?>
  </div>

  <div class="ui inverted vertical footer segment">
    <div class="ui center aligned container">
      <div class="ui stackable inverted divided grid">
        <div class="three wide column">
            Empty block
        </div>
        <div class="three wide column">
            Empty block
        </div>
        <div class="three wide column">
            Empty block
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">Information</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam felis tortor, aliquet eget tempor ut, auctor vitae massa. Suspendisse potenti. Donec ante quam, finibus a tempor non, facilisis id ligula.</p>
        </div>
      </div>
      <div class="ui inverted section divider"></div>
      <img src="<?php echo URL::base(); ?>assets/images/logo.png" class="ui centered mini image">
      <div class="ui horizontal inverted small divided link list">
        <a class="item" href="#">Site Map</a>
        <a class="item" href="#">Contact Us</a>
        <a class="item" href="#">Terms and Conditions</a>
        <a class="item" href="#">Privacy Policy</a>
      </div>
    </div>
  </div>
    <?php foreach($scripts as $script): ?>
        <script src="<?php echo URL::base(); ?>assets/js/<?php echo $script; ?>.js">
        </script>
    <?php endforeach; ?>
</body>

</html>
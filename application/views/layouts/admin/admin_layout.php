<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title><?php echo $title; ?></title>
  <meta name="description" content="<?php echo $description; ?>">

    <?php foreach($styles as $style): ?>
        <link href="<?php echo URL::base(); ?>assets/css/<?php echo $style; ?>.css" 
        rel="stylesheet" type="text/css" />
    <?php endforeach; ?>


</head>
<body>

<?php include Kohana::find_file('views/admin/includes', 'navigation'); ?>  

  <div class="ui main text container">
      <?php echo $content; ?>
  </div>

  <div class="ui inverted vertical footer segment"></div>
    <?php foreach($scripts as $script): ?>
        <script src="<?php echo URL::base(); ?>assets/js/<?php echo $script; ?>.js">
        </script>
    <?php endforeach; ?>
</body>

</html>
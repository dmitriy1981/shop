<h1 class="ui header">Categories</h1>
<hr>
<p>
<div class="ui three column grid">
    <?php foreach ($categories AS $v): ?>
        <div class="column">
         <a class="item-link" href="/admin/category/<?php echo $v['id']; ?>">
          <div class="ui segment">
            <img src="<?php echo $v['photo']; ?>" class="ui wireframe image">
          </div>
            <div class="product-name">
                <div class="ui grid">
                  <div class="eight wide column">
                      <?php echo $v['name']; ?>
                  </div>
                </div>
            </div>
         </a>
        </div>
    <?php endforeach; ?>
</div>
</p>
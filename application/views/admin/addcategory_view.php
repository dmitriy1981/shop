<p id="item-block">
<form method="POST" class="add-item-form">
    <?php if(!empty($errors)): ?>    
       <div class="ui warning message warning-message">
          <div class="header">Could you check something!</div>
          <ul class="list">
          <?php foreach($errors AS $v): ?>    
              <li><?php echo ucfirst($v); ?>.</li>
          <?php endforeach; ?>  
          </ul>
        </div>
    <?php endif; ?>    
    <div class="ui grid">
      <div class="sixteen wide column">
          <h2 id="item-title">
              <div class="ui input small product-name-field focus">
                <input type="text" value="<?php if(isset($_POST['name'])){ echo $_POST['name']; } ?>" name="name">
              </div>
          </h2>
      </div>
      <div class="ten wide column">
          <div class="sixteen wide column item-photo-block">
                  <a style="position: absolute; z-index: 300;" class="ui label add-image">
                      <i class="photo green icon"></i> 
                  Change image</a>
                  <a style="position: absolute; z-index: 300; display: none" class="ui label remove-image">
                      <i class="remove circle red icon"></i> 
                  Remove</a>
              <input style="width: 0;height: 0" type="file" id="img" >
              <img style="max-width: 300px;" class="ui medium rounded image product-image" src="/assets/images/no_image.jpg">
              <input type="hidden" value="" name="image">
          </div>
      </div>
      <div class="six wide column">
          <div style="text-align: center" class="sixteen wide column add-to-cart edit-button-block">
              <button class="ui right labeled icon button secondary add-new-category">
                <i class="right arrow icon"></i>
                Add new
              </button>
          </div>
      </div>
    </div>
    </form>
</p>
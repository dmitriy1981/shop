<p id="item-block">
    <div class="ui grid">
      <div class="sixteen wide column">
          <h2 id="item-title">
              <div class="ui input small product-name-field focus">
                <input type="text" value="<?php echo $product[0]['name']; ?>" name="name">
              </div>
          </h2>
      </div>
      <div class="ten wide column">
          <div class="sixteen wide column item-photo-block">
                  <a style="position: absolute; z-index: 300;" class="ui label add-image">
                      <i class="photo green icon"></i> 
                  Change image</a>
                  <a style="position: absolute; z-index: 300; display: none" class="ui label remove-image">
                      <i class="remove circle red icon"></i> 
                  Remove</a>
              <input style="width: 0;height: 0" type="file" id="img" >
              <img style="max-width: 300px;" class="ui medium rounded image product-image" src="<?php echo $product[0]['photo']; ?>">
          </div>
          <div class="sixteen wide column item-description">
                <div class="ui form">
                  <div class="field">
                    <label>Description</label>
                    <textarea maxlength="500" id="product-description"><?php echo $product[0]['description']; ?></textarea>
                  </div>
                </div>
          </div>
      </div>
      <div class="six wide column">
          <div id="item-id"><?php echo $product[0]['id']; ?></div>
            <div class="ui input price-input focus">
                <input id="price" 
                       value="<?php echo number_format($product[0]['price'], 2); ?>" placeholder="Price" type="text"> 
                <span>UAH</span>
            </div>
          <div style="text-align: center" class="sixteen wide column add-to-cart edit-button-block">
              Categories
                <select class="ui fluid search dropdown product-categories" multiple="">
                  <option value="">State</option>
                  <?php for ($i = 0; $i < count($categories); $i++): ?>
                  <?php $selected = ''; ?>
                  <?php foreach ($fromcategory AS $v): ?>
                        <?php if($categories[$i]['id'] == $v['category_id']): ?>
                            <?php $selected = 'selected="selected"'; ?>
                        <?php endif; ?>
                  <?php endforeach; ?>
                    <option <?php echo $selected; ?> value="<?php echo $categories[$i]['id']; ?>"><?php echo $categories[$i]['name']; ?></option>
                  <?php endfor; ?>
                </select>
              <button class="ui right labeled icon button red delete-cart-button">
                <i class="right arrow icon"></i>
                Delete
              </button>
              <button class="ui right labeled icon button secondary update-cart-button">
                <i class="right arrow icon"></i>
                Update
              </button>
          </div>
      </div>
    </div>
</p>
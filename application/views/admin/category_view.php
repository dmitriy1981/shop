<p id="item-block">
    <div class="ui grid">
      <div class="sixteen wide column">
          <h2 id="item-title">
              <div class="ui input small product-name-field focus">
                <input type="text" value="<?php echo $category[0]['name']; ?>" name="name">
              </div>
          </h2>
      </div>
      <div class="ten wide column">
          <div class="sixteen wide column item-photo-block">
                  <a style="position: absolute; z-index: 300;" class="ui label add-image">
                      <i class="photo green icon"></i> 
                  Change image</a>
                  <a style="position: absolute; z-index: 300; display: none" class="ui label remove-image">
                      <i class="remove circle red icon"></i> 
                  Remove</a>
              <input style="width: 0;height: 0" type="file" id="img" >
              <img style="max-width: 300px;" class="ui medium rounded image product-image" src="<?php echo $category[0]['photo']; ?>">
          </div>
      </div>
      <div class="six wide column">
          <div id="item-id"><?php echo $category[0]['id']; ?></div>
          <div style="text-align: center" class="sixteen wide column add-to-cart edit-button-block">
              <button class="ui right labeled icon button red delete-category-button">
                <i class="right arrow icon"></i>
                Delete
              </button>
              <button class="ui right labeled icon button secondary update-category-button">
                <i class="right arrow icon"></i>
                Update
              </button>
          </div>
      </div>
    </div>
</p>
  <div class="ui fixed inverted menu">
    <div class="ui container">
      <a href="/admin" class="header item">
        <img class="logo" src="<?php echo URL::base(); ?>assets/images/admin_avatar-80x80.png">
        Admin panel (orders)
      </a>
        <!------->
      <div class="ui simple dropdown item">
        Products <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="/admin/edit">Edit</a>
          <a class="item" href="/admin/add">Add new</a>
        </div>
      </div>
       <!------->
      <div class="ui simple dropdown item">
        Categories <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="/admin/categories">Edit</a>
          <a class="item" href="/admin/addcategory">Add new</a>
        </div>
      </div>
        <!------->
      <a href="/admin/logout" class="item">Log out</a>
    </div>
  </div>
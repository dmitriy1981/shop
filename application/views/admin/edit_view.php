<h1 class="ui header"><?php echo $categoryTitle; ?></h1>
<hr>
<p>
<div class="ui three column grid">
    <?php foreach ($products AS $v): ?>
        <div class="column">
         <a class="item-link" href="/admin/product/<?php echo $v['id']; ?>">
          <div class="ui segment">
            <img src="<?php echo $v['photo']; ?>" class="ui wireframe image">
          </div>
            <div class="product-name">
                <div class="ui grid">
                  <div class="eight wide column small-title">
                      <?php echo $v['name']; ?>
                  </div>
                  <div class="seven wide column price">
                      <?php echo number_format($v['price'],2).'<br> UAH'; ?>
                  </div>
                </div>
            </div>
         </a>
        </div>
    <?php endforeach; ?>
</div>
    <div class="ui one column grid">
        <div style="text-align: center" class="column">
            <?php echo $pagination; ?>
        </div>
        
    </div>
</p>
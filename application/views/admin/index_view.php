<h1 class="ui header">Orders</h1>
<table id="example" class="display" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Date</th>
                <th>Products</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Sum</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Products</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Sum</th>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach($orders AS $v): ?>
            <?php $products = unserialize($v['items']); ?>
            <tr>
                <td><?php echo substr($v['date'],0,16); ?></td>
                <td>
                    <?php
                    echo "<div class=\"ui bulleted list\">";
                        for ($i = 0; $i < count($products); $i++): 
                            echo "<div class=\"item\">".$products[$i]['title']." &#215; ".$products[$i]['quantity']."</div>";
                        endfor;
                        echo "<div>";
                    ?>
                </td>
                <td><?php echo $v['fname'].' '.$v['lname']; ?></td>
                <td><?php echo $v['email']; ?></td>
                <td><?php echo $v['address']; ?></td>
                <td><?php echo $v['phone']; ?></td>
                <td><?php echo round($v['summ'], 2); ?> UAH</td>
            </tr>
            <?php endforeach; ?>
        </tbody>
</table>
<?php if(!$complete): ?>
    <h1 class="ui header">Secure Checkout</h1>
<?php endif; ?>
<div class="ui grid">
 <?php if(!empty($_SESSION['cart'])): ?>  
    
    <form method="POST" class="inverted-form">   
    <?php if(!empty($errors)): ?>    
       <div class="ui warning message warning-message">
          <div class="header">Could you check something!</div>
          <ul class="list">
          <?php foreach($errors AS $v): ?>    
              <li><?php echo ucfirst($v); ?>.</li>
          <?php endforeach; ?>  
          </ul>
        </div>
    <?php endif; ?>    
    <div class="ui inverted segment">
      <div class="ui inverted form">
          
        <div class="one fields">
          <div class="field sixteen wide column">
            <label>Address</label>
            <input value="<?php if(isset($_POST['address'])){ echo $_POST['address']; } ?>" name="address" placeholder="Address" type="text">
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>First Name</label>
            <input value="<?php if(isset($_POST['fname'])){ echo $_POST['fname']; } ?>" name="fname" placeholder="First Name" type="text">
          </div>
          <div class="field">
            <label>Last Name</label>
            <input value="<?php if(isset($_POST['lname'])){ echo $_POST['lname']; } ?>" name="lname" placeholder="Last Name" type="text">
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>Telephone (380xx-xxx-xx-xx)</label>
            <input value="<?php if(isset($_POST['phone'])){ echo $_POST['phone']; } ?>" name="phone" id="phone" placeholder="Telephone" type="text">
          </div>
          <div class="field">
            <label>Email</label>
            <input value="<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>" name="email" placeholder="Email" type="text">
          </div>
        </div>
          
        <div class="inline field">
          <div class="ui checkbox">
              <input name="terms" class="hidden" tabindex="0" type="checkbox">
            <label>I agree to the terms and conditions</label>
          </div>
        </div>
        <div class="ui submit button checkout-button-complete">Submit</div>
      </div>
    </div>
    </form>
    
  <?php else: ?>
    <?php if($complete): ?>
      <div class="ui icon message success-message">
        <i class="mail outline icon"></i>
        <div class="content">
          <div class="header">
            Thank you for your order!
          </div>
          <p>Your purchase is being processed and will be completed within approximately 4-8 hours.</p>
        </div>
      </div>
<script>

    var StartTime = 1;
    
    function timer() 
    {
      document.getElementById("redirect_timer").innerHTML = StartTime;
      
      StartTime++;
      
      if(StartTime == (10 + 1))
      {
          clearInterval(intervalID);
          window.location.replace("/");
      }
    }

   var intervalID = setInterval(timer, 1000);
 
</script>

                <div class="panel-body">
                    You will be redirected to a Index page in 10 seconds
                    <strong id="redirect_timer">0</strong>
                </div>

    <?php endif; ?>
    <div class="row empty-cart">
        <h3>Shopping Cart is Empty</h3>
        <h6>You have no items in your shopping cart.</h6>
    </div>
  <?php endif; ?>  
</div>
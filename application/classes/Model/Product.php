<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Product extends Model
{
    private static $tableName = 'products';
 
    public static function getProducts($cat, $limit, $offset)
    {
        if($cat)
        {
            $query = DB::query(Database::SELECT,
                    "SELECT * FROM ".self::$tableName
                    ." WHERE id IN"
                    ."(SELECT product_id FROM product_category "
                    . "WHERE category_id = $cat) "
                    . "ORDER BY id DESC LIMIT $limit OFFSET $offset");
        }
        else
        {
            $query = DB::query(Database::SELECT,
                    "SELECT * FROM ".self::$tableName.
                    " ORDER BY id DESC LIMIT $limit OFFSET $offset");
        }
        
        return $query->execute()->as_array();
    }
    public static function getProduct($id)
    {
            $productInfo     = [];
        
            $queryProduct    = DB::query(Database::SELECT,
                    "SELECT * FROM ".self::$tableName." WHERE id = $id");
            
            $product         = $queryProduct->execute()->as_array();
            
            $queryCategories = DB::query(Database::SELECT,
                    "SELECT id, name FROM categories "
                    . "WHERE id IN(SELECT category_id "
                    . "FROM product_category WHERE product_id = $id)");
            
            $categories['categories']
                             = $queryCategories->execute()->as_array();
            
            
            return array_merge($product, $categories);
    }
    public static function getCountProducts($cat)
    {
        if($cat != 0)
        {
            $queryCount     = DB::query(Database::SELECT,
                    "SELECT COUNT(*) AS count FROM product_category WHERE category_id = $cat");   
        }
        else 
        {
            $queryCount     = DB::query(Database::SELECT,
                    "SELECT COUNT(DISTINCT product_id) AS count FROM product_category");   
        }
            
            return $queryCount->execute()->current();
    }
    public static function updateProduct($post)
    {
        $image       = $post['img'];
        $name        = $post['name'];
        $categories  = explode(',', $post['category']);
        $id          = $post['id'];
        $descr       = $post['descr'];
        $price       = floatval($post['price']);
        
                if(strpos($image, 'base64'))
                {
                    $image = self::Base64ToImage($image);
                }
                else
                {
                    $image = $post['img']; 
                }
            
            $db = Database::instance();
            $db->begin();
            
            try
            {
                // Update data
                DB::update(self::$tableName)->set(array(
                    'name'        => $name, 
                    'description' => $descr,
                    'photo'       => $image,
                    'price'       => $price,
                    ))
                    ->where('id', '=', $id)->execute();         
                
                DB::delete('product_category')
                     ->where('product_id', '=', $id)->execute();
              

                $query = DB::insert('product_category', array('product_id', 'category_id'));
                
                for($i = 0; $i < count($categories); $i++)
                {
                    $query->values(array($id, $categories[$i]));
                }
                
                    $query->execute();


                    $db->commit();
                
                return TRUE;
                
            }
            catch (Database_Exception $e)
            {
                 $db->rollback();
            }          
            
    }
    public static function addProduct($post)
    {
        $name       =   $post['name'];
        $image      =   $post['image']; 
        $categories =   explode(',', $post['category']);
        $descr      =   $post['description'];
        $price      =   floatval($post['price']);
        
                if(strpos($image, 'base64'))
                {
                    $image = self::Base64ToImage($image);
                }
                else
                {
                    $image = '/assets/images/no_image.jpg'; 
                }
                
            $db = Database::instance();
            $db->begin();
            
            try
            {
                // Insert book data
                $addQuery = DB::insert(self::$tableName,
                    array('name', 'description','photo', 'price'))
                    ->values(array($name, $descr, $image, $price))->execute();
                
                $insertId = $addQuery[0];

                // Insert headings
                $query = DB::insert('product_category', array('product_id', 'category_id'));
                
                for($i = 0; $i < count($categories); $i++)
                {
                    $query->values(array($insertId, $categories[$i]));
                }
                
                
                    $query->execute();


                    $db->commit();
                
                return TRUE;
                
            }
            catch (Database_Exception $e)
            {
                 $db->rollback();
            }    
    }
    public static function deleteProduct($id)
    {
            DB::delete(self::$tableName)
                     ->where('id', '=', $id)->execute();
    }
    //////////////////
    private static function Base64ToImage($base64)
    {
            $imageName = hash('adler32', microtime());
            $data = str_replace(' ','+',$base64);

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                
            $data = base64_decode($data);
            
               $f = finfo_open();
               
            $mime = finfo_buffer($f, $data, FILEINFO_MIME_TYPE);
            $mime = substr($mime, strpos($mime, '/') + 1);

            
            file_put_contents( DOCROOT.'/assets/images/generate/'.$imageName.'.'.$mime, $data);   
            
            return '/assets/images/generate/'.$imageName.'.'.$mime;
    }

}
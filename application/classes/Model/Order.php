<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Order extends Model
{
    private static $tableName = 'orders';
 
    public static function addOrder($data)
    {
        $totalSumm = 0;
        
        if(isset($_SESSION['cart'])):
          for($i = 0; $i < count($_SESSION['cart']); $i++):
            $totalSumm += $_SESSION['cart'][$i]['total']; 
          endfor;
        endif;  

        $query = DB::query(Database::INSERT,
                    "INSERT INTO ".self::$tableName
                    ."(items, fname, lname, email, address, summ, phone) "
                . "VALUES("
                . "'$data[order]', "
                . "'$data[fname]', "
                . "'$data[lname]', "
                . "'$data[email]', "
                . "'$data[address]', "
                . "'$totalSumm', "
                . "'$data[phone]')");
        

        
        return $query->execute();
    }
    public static function getOrders()
    {
        return DB::select()->from(self::$tableName)
                ->order_by('id', 'DESC')
                ->execute();
    }
}
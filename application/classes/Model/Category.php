<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Category extends Model
{
    private static $tableName = 'categories';
 
    /**
     * Get categories
     * @return array
     */
    public static function getCategories()
    {
        $query = DB::select_array(array('id', 'name', 'photo'))
                               ->from(self::$tableName)
                                ->order_by('id', 'DESC');
        
        return $query->execute()->as_array();
    }
    public static function getIdCategories($id)
    {
        $query = DB::select_array(array('category_id'))
                             ->from('product_category')
                        ->where('product_id', '=', $id);
        
        return $query->execute()->as_array(); 
    }
    public static function getCategory($id)
    {
        $query = DB::select_array(array('id','name', 'photo'))
                                 ->from(self::$tableName)
                                  ->where('id', '=', $id);
        
        return $query->execute()->as_array();
    }
    public static function updateCategory($post)
    {
        $id    = $post['id'];
        $image = $post['img'];
        $name  = $post['name'];
        
                if(strpos($image, 'base64'))
                {
                    $image = self::Base64ToImage($image);
                }
                else
                {
                    $image = $post['img']; 
                }
                
                DB::update(self::$tableName)->set(array(
                    'name'        => $name, 
                    'photo'       => $image,
                    ))
                    ->where('id', '=', $id)->execute();  
        
    }
    public static function deleteCategory($id)
    {
               DB::delete(self::$tableName)
                     ->where('id', '=', $id)->execute();
    }
    public static function addCategory($post)
    {
        $name  = $post['name'];
        $image = $post['image'];
        
                if(strpos($image, 'base64'))
                {
                    $image = self::Base64ToImage($image);
                }
                else
                {
                    $image = '/assets/images/no_image.jpg'; 
                }

                // Insert data
                return DB::insert(self::$tableName,
                    array('name','photo'))
                    ->values(array($name, $image))->execute();
                
    }

    //////////////////
    private static function Base64ToImage($base64)
    {
            $imageName = hash('adler32', microtime());
            $data = str_replace(' ','+',$base64);

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                
            $data = base64_decode($data);
            
               $f = finfo_open();
               
            $mime = finfo_buffer($f, $data, FILEINFO_MIME_TYPE);
            $mime = substr($mime, strpos($mime, '/') + 1);

            
            file_put_contents( DOCROOT.'/assets/images/generate/'.$imageName.'.'.$mime, $data);   
            
            return '/assets/images/generate/'.$imageName.'.'.$mime;
    }

}
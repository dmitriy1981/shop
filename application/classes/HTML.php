<?php defined('SYSPATH') OR die('No direct script access.');

class HTML extends Kohana_HTML 
{
    public static function pagination($perPage, $page, $cat, $admin = NULL)
    {
        // Общее число записей в таблице товаров
        $posts = Model_Product::getCountProducts($cat);
        $posts = $posts['count'];
        // Кол-во кнопок в пагинаторе
        $total = intval(($posts - 1) / $perPage) + 1; 

        // прячем пагинацию, если она не нужна для такого кол-ва товаров
        if($perPage >= $posts)
        {
            return NULL;
        }
        
        // Поехали создавать пагинатор!
        $paginationString = '<div class="ui pagination menu">';
        
        if($page != 0)
        {
            $paginationString .= '<a class="item" href="/category/'.$cat.'/1">&laquo; Start</a>';
        }

        for($i = 1; $i<= $total; $i++) 
        {
            if ($i - 1 == $page) 
            {
              $paginationString .= '<a title="'.$i.'" href="/category/'.$cat.'/'.$i.'" class="item active">'.$i.'</a>';
            } 
            else 
            {
              $paginationString .= '<a class="item" href="/category/'.$cat.'/'.$i.'">'.$i.'</a>';
            }
         }
         
         if(($page + 1) != $total)
         {
             $paginationString .= '<a class="item" href="/category/'.$cat.'/'.$total.'">End &raquo;</a>';
         }
         
         $paginationString .= '</div>';
         
            if($admin === TRUE)
            {  
                $paginationString = str_replace('/category/', '/admin/edit/', $paginationString);
            }
         
         return $paginationString;
    }

}

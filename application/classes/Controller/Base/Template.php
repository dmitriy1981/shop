<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Base_Template extends Controller_Template
{
    public $template   = 'layouts/main_layout';
            
    function before() 
    {
        parent::before();
        

        View::set_global('title',       'Тестовый сайт');				
        View::set_global('description', 'Сайт на Kohana');
        
        $this->template->styles  = ['semantic.min','main'];
        $this->template->scripts = [
                                    'jquery-git.min',
                                    'semantic.min',
                                    'jquery.maskedinput.min',
                                    'main'
        ];

        $this->template->content = '';
    }

} // End Base_Template
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Ajax extends Controller
{

    public function __construct(\Request $request, \Response $response) {
        
           if(!$_POST) die;
        
        parent::__construct($request, $response);
        
        if(!Auth_File::instance()->logged_in())
        {
            $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin/auth');
            exit;
        }
        
    }

    public function action_update()
    {
        $post = $this->request->post();
        
        Model_Product::updateProduct($post);
    }
    public function action_delete()
    {
        $id   = $this->request->post('id');
        
        Model_Product::deleteProduct($id);
    }
    public function action_updatecategory()
    {
        $post = $this->request->post();

        Model_Category::updateCategory($post);
    }
    public function action_deletecategory()
    {
        $id   = $this->request->post('id');
        
        Model_Category::deleteCategory($id);
    }


} // End Ajax

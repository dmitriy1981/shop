<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Main extends Controller_Template
{
    public $template   = 'layouts/admin/admin_layout';
    public $orders     = [];
    public $errors     = [];
    public $categories = [];
    public $categoryTitle = 'All products';
            
    function before() 
    {
        parent::before();
        
        if(!Auth_File::instance()->logged_in())
        {
            $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin/auth');
            exit;
        }

        View::set_global('title',       'Админпанель');				
        View::set_global('description', 'Админпанель на Kohana');
        
        $this->template->styles  = ['semantic.min',
                                'jquery-ui','dataTables.jqueryui.min','main'];
        $this->template->scripts = [
                                    'jquery-git.min',
                                    'semantic.min',
                                    'jquery.dataTables.min',
                                    'dataTables.jqueryui.min',
                                    'admin'
        ];

        $this->template->content = '';
    }
    public function __construct(\Request $request, \Response $response)
    {
        parent::__construct($request, $response);
        
       $this->categories         = Model_Category::getCategories();
    }
    
    public function action_index()
    {
        $this->orders = Model_Order::getOrders();
        
        $this->template->content = View::factory('admin/index_view')
                                      ->set('orders', $this->orders);
    }
    public function action_edit()
    {
        
        // Количество выводимых товаров
        $perPage     = 6;
        // Номер текущей страницы
        $pageNumber  = intval($this->request->param('pg'));
        
            if($pageNumber) 
            {
              $page  = $pageNumber - 1;
            }
            else
            {
              $page  = 0;
            }
        // вычисляем offset для отзывов
        $start       = ceil($page * $perPage);
        
        
        $catId       = intval($this->request->param('id'));
        
        // Хелпер вывода постраничной навигации
        $pagination  = HTML::pagination($perPage, $page, $catId, TRUE);

            foreach($this->categories AS $v)
            {
                if($v['id'] == $catId)
                {
                    $this->categoryTitle = $v['name']; break;
                }
            }
            
        $this->products = Model_Product::getProducts($catId, $perPage, $start);
        
        $this->template->content = View::factory('admin/edit_view')
                     ->set('catId',                       $catId)
                     ->set('products',           $this->products)
                     ->set('pagination',             $pagination)
                     ->set('categoryTitle', $this->categoryTitle);
    }
    public function action_add()
    {
        if($data    = Arr::sanitize($this->request->post()))
        {

            $post     = Validation::factory($data);
            $post->rule('description', 'not_empty')
                 ->rule('name'  , 'not_empty')
                 ->rule('name'  , 'alpha_numeric', array(':value', TRUE))
                 ->rule('name'  , 'min_length', array(':value', 2))
                 ->rule('name'  , 'max_length', array(':value', 20))
                 ->rule('price'  , 'not_empty')
                 ->rule('category'  , 'not_empty');      


            if(!$post->check())
            {
                $this->errors = $post->errors('validation');
            }
            

            if(empty($this->errors))
            {
                
                if(Model_Product::addProduct($data))
                {
                      $_POST  = [];
                    $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin/edit');
                }
                    
            }
        }
        
        $this->template->content = View::factory('admin/add_view')
                            ->set('errors',         $this->errors)
                            ->set('categories', $this->categories);
    }
    public function action_product()
    {
        $productId    = intval($this->request->param('id'));
        
        $product      = [];
        $product      = Model_Product::getProduct($productId);
        
        $fromCategory = [];
        $fromCategory = Model_Category::getIdCategories($productId);
                
        $this->template->content = View::factory('admin/product_view')
                                ->set('categories', $this->categories)
                                ->set('fromcategory',   $fromCategory)
                                         ->set('product',    $product);
    }
    public function action_addcategory()
    {
        if($data    = Arr::sanitize($this->request->post()))
        {

            $post     = Validation::factory($data);
           
            $post->rule('name'  , 'not_empty')
                 ->rule('name'  , 'alpha_numeric', array(':value', TRUE))
                 ->rule('name'  , 'min_length', array(':value', 1))
                 ->rule('name'  , 'max_length', array(':value', 100));      


            if(!$post->check())
            {
                $this->errors = $post->errors('validation');
            }
            

            if(empty($this->errors))
            {
                
                if(Model_Category::addCategory($data))
                {
                    $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin/categories');
                }
                    
            }
        }
        
        $this->template->content = View::factory('admin/addcategory_view')
                                            ->set('errors', $this->errors);
    }
    public function action_categories()
    {
        
        $this->template->content = View::factory('admin/categories_view')
                                   ->set('categories', $this->categories);
    }
    public function action_category()
    {
        $categoryId    = intval($this->request->param('id'));
        
        $category = Model_Category::getCategory($categoryId);
        
        $this->template->content = View::factory('admin/category_view')
                                           ->set('category', $category);
    }
} // End Admin/Main

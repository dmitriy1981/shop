<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Auth extends Controller
{
    
    public function action_index()
    {
        $post = $this->request->post();
        
        if(Auth_File::instance()->logged_in())
        {
            $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin');
            exit;
        }

        if(Auth_File::instance()->login(@$post['name'], @$post['password']))
        {
                // Авторизация прошла успешно
            $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin');
        }
        
        $data = [];
        $view = View::factory('admin/includes/auth_page', $data);
        
        $this->response->body($view);
    }
    public function action_logout()
    {
        Auth_File::instance()->logout();
        
            $this->redirect('http://'
                            .$_SERVER['SERVER_NAME'].'/admin/auth');
            exit;  
    }
}
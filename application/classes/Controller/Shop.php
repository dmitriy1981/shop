<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Shop extends Controller_Base_Template
{
    public $session;
    public $errors        = [];
    public $categories    = [];
    public $products      = [];
    public $categoryTitle = 'All products';


    public function __construct(\Request $request, \Response $response) 
    {
        parent::__construct($request, $response);
        
        $this->categories = Model_Category::getCategories();
        $this->session    = Session::instance();

        View::set_global('categories',   $this->categories);
    }

    public function action_index()
    {
        $this->template->content = View::factory('main_view');
    }
    public function action_category()
    {
        // Количество выводимых товаров
        $perPage     = 6;
        // Номер текущей страницы
        $pageNumber  = intval($this->request->param('pg'));
        
            if($pageNumber) 
            {
              $page  = $pageNumber - 1;
            }
            else
            {
              $page  = 0;
            }
        // вычисляем offset для отзывов
        $start       = ceil($page * $perPage);
        
        
        $catId       = intval($this->request->param('id'));
        
        // Хелпер вывода постраничной навигации
        $pagination  = HTML::pagination($perPage, $page, $catId);

            foreach($this->categories AS $v)
            {
                if($v['id'] == $catId)
                {
                    $this->categoryTitle = $v['name']; break;
                }
            }
            
        $this->products = Model_Product::getProducts($catId, $perPage, $start);
        
        $this->template->content = View::factory('category_view')
                     ->set('catId',                       $catId)
                     ->set('products',           $this->products)
                     ->set('pagination',             $pagination)
                     ->set('categoryTitle', $this->categoryTitle);
    }
    public function action_product()
    {
        $productId = intval($this->request->param('id'));
        
        $product   = [];
        $product   = Model_Product::getProduct($productId);
        
        
        $this->template->content = View::factory('product_view')
                                   ->set('product',    $product);
    }
    public function action_cart()
    {
        $this->template->content = View::factory('cart_view');
    }
    public function action_checkout()
    {
        $complete = FALSE;
        // Если пришел POST будем его обрабатывать
        if($data    = Arr::sanitize($this->request->post()))
        {
            $filePath = '';

            $post     = Validation::factory($data);
            $post->rule('address', 'not_empty')
                 ->rule('fname'  , 'not_empty')
                 ->rule('fname'  , 'alpha_numeric', array(':value', TRUE))
                 ->rule('fname'  , 'min_length', array(':value', 2))
                 ->rule('fname'  , 'max_length', array(':value', 20))
                 ->rule('lname'  , 'not_empty')
                 ->rule('lname'  , 'alpha_numeric', array(':value', TRUE))
                 ->rule('lname'  , 'min_length', array(':value', 2))
                 ->rule('lname'  , 'max_length', array(':value', 20))
                 ->rule('email'  , 'not_empty')
                 ->rule('email'  , 'email_domain')
                 ->rule('email'  , 'email')
                 ->rule('phone'  , 'not_empty')
                 ->rule('phone'  , 'phone');      


            if(!$post->check())
            {
                $this->errors = $post->errors('validation');
                $this->errors = str_replace(['fname','lname'], 
                                            ['first name','last name'], 
                                                        $this->errors);
            }
            
            if($this->is_empty($this->request->post('terms')))
            {
                $this->errors['terms'] = 'you must agree to the terms and conditions before registering';
            }
            if(!isset($_SESSION['cart']))
            {
                $this->errors['cart']  = 'Your session has expired';
            }

            if(empty($this->errors))
            {
                $data['order'] = serialize($_SESSION['cart']);
                // Save order
                Model_Order::addOrder($data);
                
                    unset($_SESSION['cart']);
                    unset($data);
                    $complete = TRUE;
                      $_POST  = [];
            }
        }
        
        $this->template->content = View::factory('checkout_view')
                                   ->set('complete',   $complete)
                                   ->set('errors', $this->errors);
    }
    function is_empty($var)
    { 
        return empty($var);
    }

} // End Shop

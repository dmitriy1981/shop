<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller
{
    public $session;
    public $cart;

    public function __construct(\Request $request, \Response $response) {
        
           if(!$_POST) die;
        
        parent::__construct($request, $response);
        
        $this->session = Session::instance();
        
    }

    public function action_addproduct()
    {
        $post[]       = $this->request->post();

        if(!$this->session->get('cart'))
        {
            $this->session->set('cart', $post);
        }
        else
        {
            $this->cart =  $this->session->get('cart');
            
            $count = count($this->cart);
            
            $new = TRUE;  
            
            for($i = 0; $i < $count; $i++)
            {
                if($this->cart[$i]['id'] == $post[0]['id'])
                {
                    $this->cart[$i]['total']    = $post[0]['total']    + $this->cart[$i]['total'];
                    $this->cart[$i]['quantity'] = $post[0]['quantity'] + $this->cart[$i]['quantity'];
                    
                        $new = FALSE;
                        
                    break;
                }
            }
            
            if($new == FALSE)
            {
                //$this->session->delete('cart');
                $this->session->set('cart', $this->cart);
            }
            else
            {
                $this->session->set('cart', array_merge($this->session->get('cart'), $post));
            }
        }
        
            echo $this->cartData();
    }
    public function action_removeproduct()
    {
        $this->cart = $this->session->get('cart');
        
        $post       = $this->request->post('id');
        
            unset($this->cart[$post]);
        
        $this->cart = array_values($this->cart);
          
        $this->session->set('cart', $this->cart);
        
    }
    public function action_cartupdate()
    {
        $this->cart = $this->session->get('cart');
        
        $values     = $this->request->post('values');
        
        for($i = 0; $i < count($values); $i++)
        {
            if($values[$i] == 0){ $values[$i] = 1; }
            
            $this->cart[$i]['quantity'] = $values[$i];
            $this->cart[$i]['total']    = $values[$i] * $this->cart[$i]['price'];
        }
        
        $this->session->set('cart', $this->cart);
    }
    private function cartData()
    {
        ob_start();
        
            include Kohana::find_file('views/includes', 'cartdata');
        
        return ob_get_clean();
    }


} // End Ajax

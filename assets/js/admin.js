$(document).ready(function(){
    
    $('#example').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    
    var ItemLinks = $('.item-link');
    
    ItemLinks.on('mouseover mouseout', function(){
       
    var CategoryName = $(this).children('.category-name, .product-name');
    
            if(CategoryName.css('visibility') == 'visible')
            {
                CategoryName.css('visibility','hidden');
            }
            else
            {
                CategoryName.css('visibility','visible');
            }
       
       return false;
    });
    
    $('.ui.dropdown').dropdown();
    
        $('#price').keyup(function(){
            var val = $(this).val();
            if(isNaN(val)){
                 val = val.replace(/[^0-9\.]/g,'');
                 if(val.split('.').length>2) 
                     val = val.replace(/\.+$/,"");
            }
            $(this).val(val); 
        });
        
        
        
        
   ///////////////////////////////////////////////////     
   var OldIMg = $('.product-image').attr('src');
   
   
    function readURL(input) 
    {
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        if (input.files && input.files[0])
        {
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                var reader = new FileReader();

                reader.onload = function (e) 
                {
                    $('.product-image').attr('src', e.target.result);
                    $('.add-image').css('display','none')
                    $('.remove-image').show()
                    .css('display','block');
                    $('input[name="image"]').val($('.product-image').attr('src'));
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    }
    
    
    
    var AddImageLink = $('.add-image');
    var ImageField   = $('input#img');
    
    
    AddImageLink.on('click change', function(e){
         e.preventDefault();
       ImageField.click()
    });
    
    ImageField.change(function(){
       readURL(this);
    });
    
    $('.remove-image').on('click', function(){
       
       $('.product-image').attr('src', OldIMg);
       $('.remove-image').css('display','none');
       $('.add-image').css('display','block');
       $('input[name="image"]').val('');
       return false;
    });        
        
        
        
        
        var ProductName  = $('.product-name-field input');
        var ProductPrice = $('#price');
        var ProductDescr = $('#product-description');
        var ProductCat   = $('div.product-categories select');
        var ProductImg   = $('.product-image');
        var ProductId    = $('#item-id');
        
    $('.update-cart-button').on('click', function(e){
        
        e.preventDefault();
        
                    $.ajax({
                        url: "/admin/update",
                        method: "POST",
                        async: false,
                        data:
                                'id='          + ProductId.text() + '&' +
                                'name='          + ProductName.val() + '&' +
                                'descr='         + ProductDescr.val() + '&' +
                                'img='           + ProductImg.attr('src') + '&' +
                                'category='      + ProductCat.val() + '&' +
                                'price='         + ProductPrice.val(),
                         success: function(data) {
                             alert('Updated data successfully')
                             location.reload(); 
                         }
                        });  
        
  });
                    
                    
    $('.delete-cart-button').on('click', function(e){
        
        e.preventDefault();
        
        if(confirm('Delete this product?'))
        {
                    $.ajax({
                        url: "/admin/delete",
                        method: "POST",
                        async: false,
                        data: 'id=' + ProductId.text(),
                         success: function(data) {
                             alert('Item has been removed');
                             window.location.replace("/admin/edit");
                         }
                        });  
        }
        
    });
    
    $('.add-new-product').on('click', function(e){
        
        e.preventDefault();
        
        $('input[name="category"]').val(ProductCat.val());
        
        $('.add-item-form').submit();
        
    });
    
    
    $('.update-category-button').on('click', function(e){
       
       e.preventDefault();
       
                    $.ajax({
                        url: "/admin/updatecategory",
                        method: "POST",
                        async: false,
                        data:
                                'id='          + ProductId.text() + '&' +
                                'name='          + ProductName.val() + '&' +
                                'img='           + ProductImg.attr('src'),
                         success: function(data) {
                             alert('Updated data successfully')
                             location.reload(); 
                         }
                        });  
        
    });
    
    
    $('.delete-category-button').on('click', function(e){
       
       e.preventDefault();
       
        if(confirm('Delete this category?'))
        {            
                    $.ajax({
                        url: "/admin/deletecategory",
                        method: "POST",
                        async: false,
                        data:
                                'id='          + ProductId.text(),
                         success: function(data) {
                             alert('Item has been removed');
                             window.location.replace("/admin/categories");
                         }
                        });  
        }                
        
    });
    

    
});
$(document).ready(function(){
    
    
    var ItemLinks = $('.item-link');
    
    ItemLinks.on('mouseover mouseout', function(){
       
    var CategoryName = $(this).children('.category-name, .product-name');
    
            if(CategoryName.css('visibility') == 'visible')
            {
                CategoryName.css('visibility','hidden');
            }
            else
            {
                CategoryName.css('visibility','visible');
            }
       
       return false;
    });
    
    var Bag     = $('.bag');
    var BagData = $('#shopping-cart span');
    
        Bag.attr('data-html', BagData.html()).popup({hoverable: true});
    
    $('.ui.dropdown').dropdown()
    
    var AddProductButton = $('.add-cart-button');
    
    
        AddProductButton.on('click', function(e){
            
            e.preventDefault();
            
            var ItemId   = $('#item-id').text();
            var Price    = $('#item-value').text();
            var Quantity = $('input[name="quantity"]').val();
            var Photo    = $('.item-photo-block img');
            var Title    = $('#item-title').text();
            
            
            var Total    = (Price * Quantity);
            
            var floatPhoto = Photo.clone().css({
                'position':'absolute',
                'margin-top': '-' + Photo.height() + 'px'
            });
            
            $('.item-photo-block').append(floatPhoto);
            
            floatPhoto.animate({
                    right: '-=80%',
                    opacity: '0.1',
                    height: '0',
                    width: '0'
                }, 1000, function(){
                    // Delete object
                    floatPhoto.remove();
                    
                    // Send ajax
                        $.ajax({
                            method: "POST",
                            url: "/ajax/addproduct",
                            dataType: "html",
                            data: { 
                                id: ItemId, 
                                title: Title, 
                                price: Price, 
                                quantity: Quantity,
                                total: Total,
                                photo: Photo.attr('src'),
                          }
                        }).done(function( msg ) {
                            $('.bag').attr('data-html',msg);
                            $('#item-count').html('('+ $(msg).find('#hide-total').html() +')')
                        });
                    });
                    
                });
                
                
        
        
    
        $(window.document).on('click', '.delete-section button',function(e){
           
           e.preventDefault();

           var DeleteIndex = $(this).attr('data-id');
           
           if(confirm('Are you sure you would like to remove this item from the shopping cart?'))
           {
                $.ajax({
                    method: "POST",
                    url: "/ajax/removeproduct",
                    data: { 
                        id: DeleteIndex,
                  }
                }).done(function(msg) {
                      location.reload(); 
                });
           }
            
        });
        
        var QuantityItem = $('.mini-field input');
        
        QuantityItem.on('keyup', function(){
            $(this).val($(this).val().replace (/\D/, ''));
            if($(this).val() == ''){ $(this).val(1); }
        });
        
        $('.cart-block:odd').css({
           'borderTop':'1px solid #ebebeb' 
        });
        
        var UpdateCart = $('.update-cart');
        
        UpdateCart.on('click', function(){
           
          var str = []; i = 0;
          
              QuantityItem.each(function(){
                 str[i] = $(this).val()
                 i++;
              });
              
                $.ajax({
                    method: "POST",
                    url: "/ajax/cartupdate",
                    data: { 
                        values: str,
                  }
                }).done(function(msg) {
                      location.reload(); 
                });  
            
        });
   
    $('.ui.checkbox').checkbox();
   
    var CheckoutFormButton = $('.checkout-button-complete');
    
        CheckoutFormButton.on('click', function(){
           $('form.inverted-form').submit(); 
        });
        
    $('#phone').mask('38099-999-99-99');
   
});